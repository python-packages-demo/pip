# pip

Python package installer. https://pypi.org/project/pip/

# Official documentation
* [*Installing Packages*](https://packaging.python.org/tutorials/installing-packages/)
* https://github.com/pypa/get-pip

# get-pip.py, ensurepip or pip package?
* get-pip.py seems to install up to date versions of pip and friends.
* get-pip.py is likely to work on all distribution.
* ensurepip and distribution package install older versions.
* ensurepip is (apparently) not available on Debian.
* pip distribution package may differ slightly from one to another distribution.

# Installing with get-pip.py
* https://pip.pypa.io

# ensurepip in Debian/Ubuntu
ensurepip is disabled in Debian/Ubuntu for the system python.
Python modules For the system python are usually handled by dpkg and apt-get.

    apt-get install python-<module name>

Install the python-pip package to use pip itself.  Using pip together
with the system python might have unexpected results for any system installed
module, so use it on your own risk, or make sure to only use it in virtual
environments.